import { Link } from "gatsby"
import React, { useState } from "react"
import { FaAngleRight } from "react-icons/fa"

export default function Dropdown({ subs, depth, subTitles }) {
  const [click, setClick] = useState(false)
  const [dropdown, setDropdown] = useState(false)

  const handleClick = () => setClick(!click)

  const onMouseEnter = () => {
    if (window.innerWidth <= 1020) {
      setDropdown(false)
    } else {
      setDropdown(true)
    }
  }
  const onMouseLeave = () => {
    if (window.innerWidth <= 1020) {
      setDropdown(false)
    } else {
      setDropdown(false)
    }
  }

  return (
    <>
      <ul
        onClick={handleClick}
        className={click ? "dropdown-menu clicked" : "dropdown-menu " + depth}
      >
        {subTitles != null
          ? subTitles.map((title, index) => {
              return (
                <li key={index} className="dropdown-link">
                  <Link to="/">{title}</Link>
                </li>
              )
            })
          : null}

        {subs != null
          ? subs.map(sub => {
              if (sub.secondary != null) {
                return (
                  <li
                    key={sub.contentfulid}
                    className="dropdown-link"
                    onMouseEnter={onMouseEnter}
                    onMouseLeave={onMouseLeave}
                  >
                    <Link to="/">
                      {sub.title} <FaAngleRight size="16px" />
                    </Link>
                    {dropdown && (
                      <Dropdown subTitles={sub.secondary} depth={"second"} />
                    )}
                  </li>
                )
              } else {
                return (
                  <li key={sub.contentfulid} className="dropdown-link">
                    <Link to="/">{sub.title}</Link>
                  </li>
                )
              }
            })
          : null}
      </ul>
    </>
  )
}
