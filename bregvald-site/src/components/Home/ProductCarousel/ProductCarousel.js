import React, { useState } from "react"
import Slider from "react-slick"
import TitleLine from "../../UI/TitleLine/TitleLine"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import useProductCarousel from "../../../hooks/ProductCarousel/useProductCarousel"
import "./styles/productCarousel.css"
import * as classes from "./styles/carouselDots.module.css"

const ProductCarousel = () => {
  const { productCarousel } = useProductCarousel()

  const [imageIndex, setImageIndex] = useState(2)

  const settings = {
    dots: true,
    infinite: true,
    focusOnSelect: true,
    speed: 300,
    slidesToShow: 3,
    centerMode: true,
    centerPadding: 0,
    arrows: false,
    initialSlide: 2,
    beforeChange: (current, next) => setImageIndex(next),
    appendDots: dots => (
      <div>
        <ul className={classes.dotsAction}>
          {dots.map(dot => {
            return <li className={classes.dotsList}>{dot.props.children}</li>
          })}
        </ul>
      </div>
    ),
    customPaging: i => (
      <button className={i === imageIndex ? classes.active : classes.dots}>
        {i + 1}
      </button>
    ),
  }

  return (
    <div className="carouselComponent">
      <div className="carouselHeader">
        <h1>POPULAARSED TOOTED</h1>
        <TitleLine className="carouselLine" />
      </div>

      <div className="carouselWrapper">
        <Slider {...settings}>
          {productCarousel.edges.map((product, index) => {
            return (
              <>
                <div
                  className={
                    index === imageIndex ? "slide activeSlide" : "slide"
                  }
                  key={product.node.contentfulid}
                >
                  <GatsbyImage
                    image={getImage(product.node.image)}
                    alt="CarouselProduct"
                  ></GatsbyImage>
                </div>
                <h2
                  className={
                    index === imageIndex
                      ? "carouselTitleActive"
                      : "carouselTitle"
                  }
                >
                  {product.node.title}
                </h2>
              </>
            )
          })}
        </Slider>
      </div>
    </div>
  )
}

export default ProductCarousel
