import React from "react"
import * as classes from "./styles/newProducts.module.css"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import useNewProducts from "../../../hooks/NewProducts/useNewProducts"

const NewProducts = () => {
  const { newProducts } = useNewProducts()

  return (
    <div className={classes["container"]}>
      <div className={classes["imageContainer"]}>
        <GatsbyImage
          image={getImage(newProducts.image)}
          alt="Product"
        ></GatsbyImage>
      </div>
      <div className={classes["textContainer"]}>
        <div className={classes["title"]}>
          <h2>{newProducts.title}</h2>
        </div>
        <div className={classes["info"]}>
          <p>{newProducts.text}</p>
          <button>Vaata uusi tooteid</button>
        </div>
      </div>
    </div>
  )
}

export default NewProducts
