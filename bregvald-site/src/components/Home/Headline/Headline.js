import React, { useState } from "react"
import Slider from "react-slick"
import Cards from "./Cards/Cards"
import useHeadline from "../../../hooks/Headline/useHeadline"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import * as classes from "./styles/headline.module.css"

const Headline = () => {
  const { headline } = useHeadline()
  const [imageIndex, setImageIndex] = useState(0)

  const settings = {
    dots: true,
    infinite: true,
    arrows: false,
    speed: 700,
    slidesToShow: 1,
    slidesToScroll: 1,
    beforeChange: (current, next) => setImageIndex(next),
    appendDots: dots => (
      <div>
        <ul className={classes.dotsAction}>
          {dots.map(dot => {
            return <li style={{ margin: "0 10px" }}>{dot.props.children}</li>
          })}
        </ul>
      </div>
    ),
    customPaging: i => (
      <button className={i === imageIndex ? classes.active : classes.dots}>
        {i + 1}
      </button>
    ),
  }

  return (
    <div className={classes["container"]}>
      <div className={classes["redBack"]}></div>
      <Slider {...settings}>
        {headline.edges.map(news => {
          return (
            <div className={classes["news"]} key={news.node.contenfulid}>
              <GatsbyImage
                image={getImage(news.node.image)}
                alt="Headline picture"
              ></GatsbyImage>
              <div className={classes["text"]}>
                <h1>{news.node.title}</h1>
                <h2>{news.node.subtitle}</h2>
              </div>
            </div>
          )
        })}
      </Slider>
      <Cards></Cards>
    </div>
  )
}

export default Headline
