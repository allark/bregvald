import React, { useEffect } from "react"
import { motion, useAnimation } from "framer-motion"
import { useInView } from "react-intersection-observer"
import { StaticImage } from "gatsby-plugin-image"
import * as classes from "./styles/Cards.module.css"

const Cards = () => {
  const { ref, inView } = useInView({
    threshold: 0.1,
  })
  const animation = useAnimation()

  const variants = {
    hidden: { opacity: 0, y: 30 },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 0.5,
      },
    },
  }

  useEffect(() => {
    if (inView) {
      animation.start("visible")
    }
  }, [inView, animation])

  return (
    <div className={classes["alignBlock"]}>
      <motion.div
        variants={variants}
        initial="hidden"
        animate={animation}
        ref={ref}
        className={classes["features"]}
      >
        <div className={classes["feature"]}>
          <StaticImage
            src="../../../../images/icons8-categorize-100.png"
            alt="Sortiment"
            placeholder="tracedSVG"
          />
          <h2>LAI TOOTEVALIK</h2>
        </div>
        <div className={classes["splitLine"]}></div>
        <div className={classes["feature"]}>
          <StaticImage
            src="../../../../images/icons8-end-call-male-100.png"
            alt="Sortiment"
            placeholder="tracedSVG"
          />
          <h2>PERSONAALNE NÕUSTAMINE</h2>
        </div>
        <div className={classes["splitLine"]}></div>
        <div className={classes["feature"]}>
          <StaticImage
            src="../../../../images/icons8-truck-100.png"
            alt="Sortiment"
            placeholder="tracedSVG"
          />
          <h2>TRANSPORT ÜLE EESTI</h2>
        </div>
      </motion.div>
    </div>
  )
}

export default Cards
