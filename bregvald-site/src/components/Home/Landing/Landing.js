import React from "react"
import DesktopLanding from "./Desktop/DesktopLanding"
import MobileLanding from "./Mobile/MobileLanding"
import * as classes from "./styles/landing.module.css"

const Landing = props => {

  return (
    <section className={classes["landingContainer"]}>
      <div className={classes.back}></div>
      <DesktopLanding onClick={props.onClick} />
      <MobileLanding onClick={props.onClick} />
    </section>
  )
}

export default Landing
