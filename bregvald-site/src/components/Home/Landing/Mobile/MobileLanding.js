import React from "react"
import useLanding from "../../../../hooks/Landing/useLanding"
import * as classes from "./styles/mobileLanding.module.css"
import { StaticImage } from "gatsby-plugin-image"
import { FiChevronDown } from "react-icons/fi"

const MobileLanding = props => {
  const { handleClick } = useLanding(props)

  return (
    <article className={classes["container"]}>
      <StaticImage
        src="https://metaforma.ee/wp-content/uploads/2020/01/K352-349.jpg"
        alt="lol"
        className={classes["image"]}
        placeholder="blurred"
      ></StaticImage>
      <div className={classes["cover"]}>
        <div className={classes["heading"]}>
          <h2>Suurim Valik</h2>
          <h1>Mööbli- ja ehitusplaate</h1>
          <h2>Lõuna-Eestis</h2>
        </div>
        <button onClick={handleClick} className={classes["button"]}>
          Tutvu Meie Tootevalikuga
        </button>
        <button className={classes["arrowDown"]}>
          <FiChevronDown
            size="70px"
            className={classes["arrow"]}
          ></FiChevronDown>
          <FiChevronDown
            size="70px"
            className={classes["arrow"]}
          ></FiChevronDown>
        </button>
      </div>
    </article>
  )
}

export default MobileLanding
