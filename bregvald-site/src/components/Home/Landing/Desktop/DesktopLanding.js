import React from "react"
import DesktopHeader from "./DesktopHeader/DesktopHeader"
import DesktopMenu from "./DesktopMenu/DesktopMenu"
import useLanding from "../../../../hooks/Landing/useLanding"
import * as classes from "./styles/desktopLanding.module.css"

const DesktopLanding = props => {
  const { handleClick } = useLanding(props)
  return (
    <article className={classes["container"]}>
    <DesktopMenu/>
      <div className={classes["inner"]}>
        <DesktopHeader />
        <button onClick={handleClick} className={classes["downIndicator"]}>
          <div className={classes["mouse"]}>
            <span></span>
          </div>
          <div className={classes["arrow"]}>
            <span></span>
          </div>
        </button>
      </div>
    </article>
  )
}

export default DesktopLanding
