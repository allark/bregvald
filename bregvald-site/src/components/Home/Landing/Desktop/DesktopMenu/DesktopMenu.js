import React, { useState, useEffect } from 'react'
import Slider from "react-slick"
import { motion, useAnimation } from "framer-motion"
import { useInView } from "react-intersection-observer"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import useLandingMenu from '../../../../../hooks/Landing/useLandingMenu'
import useLandingBack from '../../../../../hooks/LandingBack/useLandingBack'
import * as classes from "./styles/desktopMenu.module.css"

const DesktopMenu = () => {
    const { landingMenu } = useLandingMenu()
    const { landingMenuBack } = useLandingBack()
    const [imageIndex, setImageIndex] = useState(0)

    const { ref, inView } = useInView({
        threshold: 0.1,
      })

    const animation = useAnimation()
    
    const variants = {
        hidden: { opacity: 0, y: 40 },
        visible: {
          opacity: 1,
          y: 0,
          transition: {
            duration: 0.5,
          },
        },
      }
    
      useEffect(() => {
        if (inView) {
          animation.start("visible")
        }
      }, [inView, animation])

    const settings = {
        className:`${classes.menuSlide}`,
        dots: true,
        infinite: true,
        arrows: false,
        speed: 700,
        fade: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        vertical: true,
        verticalSwiping: true,
        beforeChange: (current, next) => setImageIndex(next),
        appendDots: dots => (
          <div>
            <ul className={classes.dotsAction}>
              {dots.map(dot => {
                return (
                
                    <li style={{ margin: "5px 0" }}>{dot.props.children}</li>
            
                )
              })}
            </ul>
          </div>
        ),
        customPaging: i => (
            <div>
                <h2 className={i === imageIndex ? classes.ident : classes.noid}>{"0"+(i+1)}</h2>
                <button className={i === imageIndex ? classes.active : classes.dots}>
                    {i + 1}
                </button>
            </div>
          
        ),
      }

    return (
        <motion.div variants={variants} initial="hidden" animate={animation} className={classes.container} ref={ref}>
            <Slider {...settings}>
                {landingMenu.edges.map(item => {
                    return (
                        <div className={classes.menu} key={item.node.contentfulid}>
                            <GatsbyImage
                                image={getImage(item.node.image)}
                                alt="Menu picture"
                                className={classes.image}
                            ></GatsbyImage>
                            <div className={classes.info}>
                                <div className={classes.circle}>
                                    <h2>{item.node.procent + "%"}</h2>
                                </div>
                                <h2 className={classes.title} style={{ marginTop: (item.node.contentfulid * 31) + 6 +  "px" }}>{item.node.title}</h2>
                            </div>
                        </div>
                    )
                })}
            </Slider>
            <div className={classes.smoke}>
              <GatsbyImage style={{zIndex:"0"}} image={getImage(landingMenuBack.image)}></GatsbyImage>
            </div>
        </motion.div>
    )
}

export default DesktopMenu
