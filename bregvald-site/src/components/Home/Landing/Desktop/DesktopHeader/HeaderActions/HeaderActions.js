import React, { useEffect } from "react"
import { motion, useAnimation } from "framer-motion"
import { useInView } from "react-intersection-observer"
import * as classes from "./styles/HeaderActions.module.css"
import { FaFacebookF, FaInstagram, FaRegUser } from "react-icons/fa"

const HeaderActions = () => {
  const { ref, inView } = useInView({
    threshold: 0.1,
  })
  const animation = useAnimation()

  const variants = {
    hidden: { opacity: 0, y: 30 },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 0.5,
      },
    },
  }

  useEffect(() => {
    if (inView) {
      animation.start("visible")
    }
  }, [inView, animation])

  return (
    <motion.div
      variants={variants}
      initial="hidden"
      animate={animation}
      ref={ref}
      className={classes["headingHelper"]}
    >
      <div className={classes["socialMedia"]}>
        <a
          href="https://instagram.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FaRegUser size="25px" color="#c11f27" />
        </a>
      </div>
      <div className={classes["splitLine"]}></div>
      <div className={classes["socialMedia"]}>
        <a
          href="https://facebook.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FaFacebookF size="25px" color="#c11f27" />
        </a>
        <a
          href="https://instagram.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FaInstagram size="30px" color="#c11f27" />
        </a>
      </div>
    </motion.div>
  )
}

export default HeaderActions
