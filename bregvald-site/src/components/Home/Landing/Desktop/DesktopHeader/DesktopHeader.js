import React, { useEffect } from "react"
import { motion, useAnimation } from "framer-motion"
import { useInView } from "react-intersection-observer"
import * as classes from "./styles/DesktopHeader.module.css"
import HeaderActions from "./HeaderActions/HeaderActions"
import TitleLine from "../../../../UI/TitleLine/TitleLine"

const DesktopHeader = () => {
  const { ref, inView } = useInView({
    threshold: 0.1,
  })
  const animation = useAnimation()

  const variants = {
    hidden: { opacity: 0, y: 40 },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        duration: 0.5,
      },
    },
  }

  const variantsButton = {
    hidden: { opacity: 0, y: 40 },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        delay: 0.3,
        duration: 0.5,
      },
    },
  }

  useEffect(() => {
    if (inView) {
      animation.start("visible")
    }
  }, [inView, animation])

  return (
    <div className={classes.leftColumn}>
      <h1>ILU JA KVALITEET</h1>
      <TitleLine className={classes.titleLine} />
      <h1>SINU MÖÖBLISSE.</h1>
      <motion.h2
        variants={variants}
        initial="hidden"
        animate={animation}
        ref={ref}
      >
        Bregvald OÜ põhiliseks tegevusalaks on ehitus- ja mööbliplaadi,
        mööblifurnituuri ja servakandi hulgimüük.
      </motion.h2>
      <HeaderActions />
      <motion.button
        variants={variantsButton}
        initial="hidden"
        animate="visible"
        className={classes["shopButton"]}
      >
        POODI
      </motion.button>
    </div>
  )
}

export default DesktopHeader
