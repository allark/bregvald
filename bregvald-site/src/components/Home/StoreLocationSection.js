import React from "react"
import * as classes from "../../styles/map.module.css"
import { GoogleMap, useJsApiLoader, Marker } from "@react-google-maps/api"
import MapStyles from "./MapStyles"

const mapContainerStyle = {
  width: "100%",
  height: "100%",
  borderRadius: "2rem",
  zIndex: 1,
}
const center = {
  lat: 58.32076,
  lng: 26.71447,
}
const options = {
  disableDefaultUI: true,
  zoomControl: true,
  styles: MapStyles,
}
export default function Store() {
  const { isLoaded, loadError } = useJsApiLoader({
    googleMapsApiKey: process.env.GOOGLE_MAPS_STATIC_API_KEY,
  })

  if (loadError) return "Error loading maps"
  if (!isLoaded) return "Loading maps"

  return (
    <div className={classes["container"]}>
      <div className={classes["mapContainer"]}>
        <div className={classes["text"]}>
          <h1>MEIE POOD.</h1>
          <h2>KAUBA TEE 5, TÕRVANDI</h2>
          <h3 className={classes["phone"]}>+372 555 98055</h3>
          <h3>E-R 8.30.-17.00</h3>
        </div>
        <GoogleMap
          mapContainerStyle={mapContainerStyle}
          zoom={15}
          center={center}
          options={options}
          key={process.env.GOOGLE_MAPS_STATIC_API_KEY}
        >
          <Marker position={center}></Marker>
        </GoogleMap>
        <div className={classes["redBack"]}></div>
      </div>
    </div>
  )
}
