import React from "react"
import useDiscount from "../../../hooks/Discount/useDiscount"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import * as classes from "./styles/Discount.module.css"

const Discount = () => {
  const { product } = useDiscount()

  return (
    <div className={classes["container"]}>
      <div className={classes["imageContainer"]}>
        <GatsbyImage
          image={getImage(product.image)}
          alt="Discount picture"
        ></GatsbyImage>
      </div>
      <div className={classes["textContainer"]}>
        <div className={classes["title"]}>
          <h2>Lõpumüük</h2>
        </div>
        <div className={classes["info"]}>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam,
            nobis?
          </p>
          <button>Vaata lisaks</button>
        </div>
      </div>
    </div>
  )
}

export default Discount
