import React from "react"
import CategoryWrapper from "./CategoryWrapper"
import * as classes from "./styles/desktopCat.module.css"

const DesktopCategories = ({ firstCategories, secondCategories }) => {
  return (
    <>
      <div className={classes["container"]}>
        <div className={classes["line"]}>
          <div className={classes["halfRedLine"]}></div>
          <div className={classes["redLine"]}></div>
        </div>
        <CategoryWrapper categories={firstCategories} />
        <CategoryWrapper categories={secondCategories} />
      </div>
    </>
  )
}

export default DesktopCategories
