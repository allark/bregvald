import React, { useEffect } from "react"
import { motion, useAnimation } from "framer-motion"
import { useInView } from "react-intersection-observer"
import { Link } from "gatsby"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import * as classes from "./styles/categoryLeft.module.css"
import TitleLine from "../../../UI/TitleLine/TitleLine"

const CategoryLeft = ({ category }) => {
  const { ref, inView } = useInView({
    threshold: 0.1,
  })
  const [refPic] = useInView({
    threshold: 0.1,
  })
  const [refText] = useInView({
    threshold: 0.1,
  })
  const animation = useAnimation()

  const variants = {
    hidden: { opacity: 0, x: -100 },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        delay: 0.2,
        duration: 0.4,
      },
    },
  }

  const variantsPic = {
    hidden: { opacity: 0, x: 100 },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        duration: 0.4,
      },
    },
  }

  const variantsText = {
    hidden: { opacity: 0, y: 30 },
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        delay: 0.4,
        duration: 0.4,
      },
    },
  }

  useEffect(() => {
    if (inView) {
      animation.start("visible")
    }
  }, [inView, animation])

  return (
    <div className={classes["inner"]}>
      <motion.div
        variants={variants}
        initial="hidden"
        animate={animation}
        ref={ref}
        className={classes["textContainer"]}
      >
        <div className={classes["text"]}>
          <h1>{category.node.title + "."}</h1>
          <TitleLine delay={0.2} className={classes.titleLine} />
          <motion.div
            variants={variantsText}
            initial="hidden"
            animate={animation}
            ref={refText}
          >
            <h2>
              Lorem ipsum dolor sit amet consectetur, adipisicing elit.
              Laboriosam, blanditiis, dolor sit amet.
            </h2>
            <Link to={"/" + category.node.to}>
              <button>VAATA LÄHEMALT</button>
            </Link>
          </motion.div>
        </div>
      </motion.div>
      <motion.div
        variants={variantsPic}
        initial="hidden"
        animate={animation}
        ref={refPic}
        className={classes["imageContainer"]}
      >
        <GatsbyImage
          image={getImage(category.node.image)}
          alt="Category"
          className={classes["image"]}
        ></GatsbyImage>
      </motion.div>
    </div>
  )
}

export default CategoryLeft
