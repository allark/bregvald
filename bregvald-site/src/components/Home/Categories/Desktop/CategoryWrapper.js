import React from "react"
import CategoryLeft from "./CategoryLeft"
import CategoryRight from "./CategoryRight"
import * as classes from "./styles/categoryWrapper.module.css"

const CategoryWrapper = ({ categories }) => {
  return (
    <div className={classes["wrapper"]}>
      <CategoryLeft category={categories[0]} />
      <div className={classes["darkBox"]}></div>
      <CategoryRight category={categories[1]} />
    </div>
  )
}

export default CategoryWrapper
