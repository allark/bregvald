import React from "react"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import { Link } from "gatsby"
import { FiPlus } from "react-icons/fi"
import * as classes from "./styles/mobileCat.module.css"

const MobilePictures = ({ categories }) => {
  return (
    <>
      <div className={classes["simplePictures"]}>
        <div className={classes["redLine"]}></div>
        <div className={classes["redLineHalf"]}></div>

        {categories.edges.map((product, index) => {
          return (
            <div className={classes["rectPicture"]} key={index}>
              <Link to={"/" + product.node.to}>
                <GatsbyImage
                  image={getImage(product.node.image)}
                  alt="Product"
                  className={classes["image"]}
                ></GatsbyImage>
                <div className={classes["textRight"]}>
                  <h3>{product.node.title}</h3>
                </div>
                <FiPlus
                  color="#c11f27"
                  size="50px"
                  className={classes["plus"]}
                />
                <div className={classes["detail"]}></div>
              </Link>
            </div>
          )
        })}
      </div>
    </>
  )
}

export default MobilePictures
