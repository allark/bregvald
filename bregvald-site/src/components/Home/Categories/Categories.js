import React from "react"
import useCategories from "../../../hooks/Categories/useCategories"
import useCategoriesDesktop from "../../../hooks/Categories/useCategoriesDesktop"
import MobilePictures from "./MobilePictures"
import DesktopCategories from "./Desktop/DesktopCategories"

const Categories = () => {
  const { categories } = useCategories()
  const { firstCategories, secondCategories } = useCategoriesDesktop()

  return (
    <>
      <MobilePictures categories={categories} />
      <DesktopCategories
        firstCategories={firstCategories}
        secondCategories={secondCategories}
      />
    </>
  )
}

export default Categories
