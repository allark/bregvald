import React from "react"
import Navigationbar from "../NavigationBar/Navbar/Navbar"
import Footer from "../Footer/Footer"
import "./styles/layout.css"

export default function Layout(props) {
  return (
    <div className="layout">
      <Navigationbar />
      <div className="content">{props.children}</div>
      <Footer />
    </div>
  )
}
