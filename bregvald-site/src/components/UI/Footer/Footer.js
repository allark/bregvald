import React from "react"
import FooterContent from "./FooterContent"
import * as classes from "./styles/footer.module.css"

export default function Footer() {
  return (
    <>
      <footer>
        <FooterContent />
      </footer>
    </>
  )
}
