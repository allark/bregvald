import React from "react"
import Slider from "react-slick"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import usePreFooter from "../../../hooks/PreFooter/usePreFooter"
import * as classes from "./styles/preFooter.module.css"

const PreFooter = () => {
  const { preFooterLogos } = usePreFooter()

  const settings = {
    infinite: true,
    arrows: false,
    slidesToShow: 2,
    autoplay: true,
    autoplaySpeed: -1000,
    speed: 15000,
    cssEase: "linear",
  }

  return (
    <>
      <div className={classes["preFooterContainer"]}>
        <Slider {...settings}>
          {preFooterLogos.edges.map(logo => {
            return (
              <div className={classes.logo}>
                <GatsbyImage
                  image={getImage(logo.node.image)}
                  alt="Logo"
                ></GatsbyImage>
              </div>
            )
          })}
        </Slider>
      </div>
    </>
  )
}

export default PreFooter
