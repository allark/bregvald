import React from "react"
import * as classes from "./styles/footerContent.module.css"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import useCategories from "../../../hooks/Categories/useCategories"
import { FaFacebookF, FaInstagram } from "react-icons/fa"
import { FiMail } from "react-icons/fi"

const FooterContent = () => {
  const { categories } = useCategories()

  return (
    <section className={classes["footerContent"]}>
      <article className={classes["inner"]}>
        <div className={classes["icons"]}>
          <div className={classes["logo"]}>
            <Link to="/">
              <StaticImage
                src="../../../images/bregvaldLogo2.png"
                alt="Logo"
                placeholder="tracedSVG"
                layout="fullWidth"
              ></StaticImage>
            </Link>
          </div>
          <div className={classes["icon"]}>
            <a
              href="https://facebook.com"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaFacebookF size="25px" color="#c11f27" />
            </a>
            <a
              href="https://instagram.com"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FaInstagram size="30px" color="#c11f27" />
            </a>
            <a
              href="https://instagram.com"
              target="_blank"
              rel="noopener noreferrer"
            >
              <FiMail size="30px" color="#c11f27" />
            </a>
          </div>
          <div className={classes["cert"]}>
            <StaticImage
              src="../../../images/Group 43.png"
              alt="Cert"
              placeholder="tracedSVG"
              layout="fullWidth"
            ></StaticImage>
          </div>
        </div>
        <div className={classes["infoWrapper"]}>
          <article className={classes["sectionWrapper"]}>
            <div className={classes["section"]}>
              <h3>Tooted</h3>
              <ul>
                {categories.edges.map(category => {
                  return (
                    <li key={category.node.contentfulid}>
                      <Link
                        to={"/" + category.node.to}
                        activeClassName={classes["active"]}
                      >
                        {category.node.title}
                      </Link>
                    </li>
                  )
                })}
              </ul>
            </div>
            <div className={classes["section"]}>
              <h3>Bregvald</h3>
              <ul>
                <li>
                  <Link to={"/kontakt"} activeClassName={classes["active"]}>
                    Kontakt
                  </Link>
                </li>
                <li>KKK</li>
                <li>+372 5559 8055</li>
                <li>info@bregvald.ee</li>
              </ul>
            </div>
          </article>
          <div className={classes["email"]}>
            <h3>Hoia end meie pakkumistega kursis</h3>
            <input type="text" placeholder="Sisestage enda e-maili aadress" />
            <button className={classes["button"]}>LIITU MEIEGA</button>
          </div>
        </div>
      </article>
    </section>
  )
}

export default FooterContent
