import React, { useEffect } from "react"
import * as classes from "./TitleLine.module.css"
import { motion, useAnimation } from "framer-motion"
import { useInView } from "react-intersection-observer"

const TitleLine = props => {
  const { ref, inView } = useInView({
    threshold: 0.1,
  })
  const animation = useAnimation()

  const lineVariants = {
    hidden: { opacity: 0, x: -200 },
    visible: {
      opacity: 1,
      x: 0,
      transition: {
        duration: 0.5,
        delay: props.delay,
      },
    },
  }

  useEffect(() => {
    if (inView) {
      animation.start("visible")
    }
  }, [inView, animation])

  return (
    <motion.div
      variants={lineVariants}
      initial="hidden"
      animate={animation}
      className={`${classes.line} ${props.className}`}
      ref={ref}
    ></motion.div>
  )
}

export default TitleLine
