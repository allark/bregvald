import React from "react"
import { AiOutlineGlobal } from "react-icons/ai"
import LanguageSelector from "./LanguageSelector"
import * as classes from "./styles/navlanguage.module.css"

const NavigationMobileLanguage = () => {
  return (
    <section className={classes["item"]}>
      <AiOutlineGlobal size="30px" color="white" />
      <LanguageSelector />
    </section>
  )
}

export default NavigationMobileLanguage
