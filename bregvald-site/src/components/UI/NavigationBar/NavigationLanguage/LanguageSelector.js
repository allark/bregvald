import React from "react"
import * as classes from "./styles/navlanguage.module.css"

const LanguageSelector = () => {
  return (
    <div className={classes["container"]}>
      <button className={classes["button"]}>EE</button>
      <div className={classes["line"]}></div>
      <button className={classes["button"]}>RU</button>
      <div className={classes["line"]}></div>
      <button className={classes["button"]}>ENG</button>
    </div>
  )
}

export default LanguageSelector
