import React from "react"
import NavItems from "./NavItems"
import NavigationMobileLanguage from "../NavigationLanguage/NavigationMobileLanguage"
import * as classes from "./styles/navitems.module.css"

const NavigationItemsList = props => {
  return (
    <ul
      className={`${classes["menu"]} ${!props.mobileMenu || classes.toggled}`}
    >
      {props.headers.map(header => {
        return (
          <NavItems
            key={header.contentfulid}
            title={header.title}
            subs={header.sub}
            to={header.to}
          />
        )
      })}
      <NavigationMobileLanguage />
    </ul>
  )
}

export default NavigationItemsList
