import { Link } from "gatsby"
import React from "react"
import * as classes from "./styles/navitems.module.css"

export default function NavItems({ title, subs, to }) {
  if (subs != null) {
    return (
      <li className={classes["item"]}>
        <Link to={"/" + to} activeClassName={classes["active"]}>
          {title}
        </Link>
      </li>
    )
  } else {
    return (
      <li className={classes["item"]}>
        <Link to={"/" + to} activeClassName={classes["active"]}>
          {title}
        </Link>
      </li>
    )
  }
}
