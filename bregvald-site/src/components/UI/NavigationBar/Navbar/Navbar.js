import React, { useState, useEffect } from "react"
import NavigationLogo from "./NavigationLogo"
import NavigationMobileMenu from "./NavigationMobileMenu"
import NavigationItemsList from "../NavItems/NavigationItemsList"
import NavigationIcons from "./NavigationIcons"
import { useStaticQuery, graphql } from "gatsby"
import * as classes from "./styles/navbar.module.css"

const query = graphql`
  {
    allContentfulNavbarMenuItems(sort: { fields: contentfulid, order: ASC }) {
      nodes {
        contentfulid
        title
        to
        sub {
          title
          secondary
          contentfulid
        }
      }
    }
  }
`

export default function NavigationBar() {
  const data = useStaticQuery(query)
  const headers = data.allContentfulNavbarMenuItems.nodes
  const [click, setClick] = useState(false)
  const [size, setSize] = useState(0)
  const [isActive, setIsActive] = useState(false)
  const [scrolled, setScrolled] = useState(0)

  if (typeof window !== "undefined") {
    let height = window.innerHeight * 0.01
    document.documentElement.style.setProperty("--vh", `${height}px`)
  }

  const mobileMenuClickHandler = () => {
    setClick(!click)
    setIsActive(!isActive)
  }

  const checkSize = () => {
    setSize(window.innerWidth)
  }

  const checkScroll = () => {
    setScrolled(window.scrollY)
  }

  useEffect(() => {
    document.body.classList.toggle("nav-mobile-toggled", click)
  }, [click])

  useEffect(() => {
    window.addEventListener("resize", checkSize)
    if (size > 1024) {
      setClick(false)
    }
    return () => {
      window.removeEventListener("resize", checkSize)
    }
  }, [size])

  useEffect(() => {
    window.addEventListener("scroll", checkScroll)
    if (scrolled >= 80) {
      setIsActive(true)
    } else if (!click) {
      setIsActive(false)
    }
    return () => {
      window.removeEventListener("scroll", checkScroll)
    }
  }, [scrolled, click])

  return (
    <nav className={`${classes["navbar"]} ${!isActive || classes.active}`}>
      <div className={classes["container"]}>
        <div className={classes["inner"]}>
          <NavigationLogo />
          <NavigationMobileMenu
            onMobileMenuClick={mobileMenuClickHandler}
            mobileMenu={click}
          />
          <NavigationItemsList mobileMenu={click} headers={headers} />
          <NavigationIcons />
        </div>
      </div>
    </nav>
  )
}
