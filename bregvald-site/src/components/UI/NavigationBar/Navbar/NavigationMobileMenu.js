import React from "react"
import { MdMenu } from "react-icons/md"
import { VscChromeClose } from "react-icons/vsc"
import * as classes from "./styles/navmobilemenu.module.css"

const NavigationMobileMenu = props => {
  const mobileMenuClickHandler = () => {
    props.onMobileMenuClick()
  }

  return (
    <div className={classes["menu"]} onClick={mobileMenuClickHandler}>
      {props.mobileMenu ? (
        <VscChromeClose size="35px" />
      ) : (
        <MdMenu size="35px" />
      )}
    </div>
  )
}

export default NavigationMobileMenu
