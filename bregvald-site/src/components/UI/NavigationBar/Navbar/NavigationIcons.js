import React from "react"
import * as classes from "./styles/navigationIcons.module.css"
import { AiOutlineSearch } from "react-icons/ai"
import { VscGlobe } from "react-icons/vsc"

const NavigationIcons = () => {
  return (
    <div className={classes["icons"]}>
      <button>
        <AiOutlineSearch size="30px" className={classes.image} />
      </button>
      <button>
        <VscGlobe size="30px" className={classes.image} />
      </button>
    </div>
  )
}

export default NavigationIcons
