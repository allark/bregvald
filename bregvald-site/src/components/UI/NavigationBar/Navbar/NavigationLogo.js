import React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import * as classes from "./styles/navlogo.module.css"

const NavigationLogo = () => {
  return (
    <div className={classes["logo"]}>
      <Link to="/">
        <StaticImage
          src="../../../../images/bregvaldLogo2.png"
          alt="Logo"
          placeholder="tracedSVG"
          layout="fullWidth"
        ></StaticImage>
      </Link>
    </div>
  )
}

export default React.memo(NavigationLogo)
