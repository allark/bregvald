import { useStaticQuery, graphql } from "gatsby"

const useCategoriesDesktop = () => {
  const data = useStaticQuery(graphql`
    query Categories {
      allContentfulHomePageProducts(
        sort: { fields: contentfulid, order: ASC }
      ) {
        edges {
          node {
            contentfulid
            title
            to
            class
            image {
              gatsbyImageData(
                quality: 90
                layout: FIXED
                placeholder: BLURRED
                width: 900
              )
            }
          }
        }
      }
    }
  `)
  const categories = data.allContentfulHomePageProducts
  const firstCategories = categories.edges.slice(0, 2)
  const secondCategories = categories.edges.slice(2, 4)

  return { firstCategories, secondCategories }
}

export default useCategoriesDesktop
