import { useStaticQuery, graphql } from "gatsby"

const useCategories = () => {
  const data = useStaticQuery(graphql`
    query Products {
      allContentfulHomePageProducts(
        sort: { fields: contentfulid, order: ASC }
      ) {
        edges {
          node {
            contentfulid
            title
            to
            class
            image {
              gatsbyImageData(
                quality: 90
                layout: CONSTRAINED
                placeholder: BLURRED
                height: 300
                width: 700
              )
            }
          }
        }
      }
    }
  `)
  const categories = data.allContentfulHomePageProducts

  return { categories }
}

export default useCategories
