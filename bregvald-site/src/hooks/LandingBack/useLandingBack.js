import { useStaticQuery, graphql } from "gatsby"

const useLandingBack = () => {
  const data = useStaticQuery(graphql`
    query LandingMenuBack {
        contentfulHomePageMenuBackground {
            image {
              gatsbyImageData(quality: 90, placeholder: BLURRED, layout: FIXED, width: 1100)
            }
          }
    }
  `)
  const landingMenuBack = data.contentfulHomePageMenuBackground
  return { landingMenuBack }
}

export default useLandingBack