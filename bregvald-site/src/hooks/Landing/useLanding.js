const useLanding = props => {
  const handleClick = () => {
    props.onClick()
  }
  return { handleClick }
}

export default useLanding
