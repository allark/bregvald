import { useStaticQuery, graphql } from "gatsby"

const useLandingMenu = () => {
  const data = useStaticQuery(graphql`
    query LandingMenu {
        allContentfulHomePageMenu(sort: {fields: contentful_id, order: ASC}) {
            edges {
              node {
                contentfulid
                title
                image {
                  gatsbyImageData(quality: 90, placeholder: BLURRED, height: 600, layout: FIXED)
                }
                procent
              }
            }
          }
    }
  `)
  const landingMenu = data.allContentfulHomePageMenu
  return { landingMenu }
}

export default useLandingMenu