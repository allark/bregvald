import { useStaticQuery, graphql } from "gatsby"

const useDiscount = () => {
  const data = useStaticQuery(graphql`
    query Discount {
      contentfulHomePageDiscount {
        id
        image {
          gatsbyImageData(
            layout: CONSTRAINED
            width: 160
            height: 350
            placeholder: BLURRED
            quality: 90
          )
        }
      }
    }
  `)
  const product = data.contentfulHomePageDiscount

  return { product }
}

export default useDiscount
