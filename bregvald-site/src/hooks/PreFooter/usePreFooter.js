import { useStaticQuery, graphql } from "gatsby"

const usePreFooter = () => {
  const data = useStaticQuery(graphql`
    query PreFooter {
      allContentfulPreFooterLogos {
        edges {
          node {
            contentfulid
            image {
              gatsbyImageData(layout: FIXED, quality: 90, height: 100)
            }
          }
        }
      }
    }
  `)
  const preFooterLogos = data.allContentfulPreFooterLogos
  return { preFooterLogos }
}

export default usePreFooter
