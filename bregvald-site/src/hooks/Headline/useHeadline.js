import { useStaticQuery, graphql } from "gatsby"

const useHeadline = () => {
  const data = useStaticQuery(graphql`
    query Headline {
      allContentfulHomePageTrends(sort: { fields: contentfulid, order: ASC }) {
        edges {
          node {
            contentfulid
            title
            subtitle
            image {
              gatsbyImageData(
                height: 425
                width: 1600
                placeholder: BLURRED
                quality: 90
                layout: FIXED
              )
            }
          }
        }
      }
    }
  `)
  const headline = data.allContentfulHomePageTrends
  return { headline }
}

export default useHeadline
