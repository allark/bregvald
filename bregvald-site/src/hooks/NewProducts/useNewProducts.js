import { useStaticQuery, graphql } from "gatsby"

const useNewProducts = () => {
  const data = useStaticQuery(graphql`
    query NewProducts {
      contentfulHomePageNewProducts {
        contentfulid
        title
        text
        image {
          gatsbyImageData(
            quality: 90
            placeholder: BLURRED
            layout: FIXED
            width: 300
            height: 350
          )
        }
      }
    }
  `)
  const newProducts = data.contentfulHomePageNewProducts
  return { newProducts }
}

export default useNewProducts
