import { useStaticQuery, graphql } from "gatsby"

const useProductCarousel = () => {
  const data = useStaticQuery(graphql`
    query ProductCarousel {
      allContentfulHomePageCarousel {
        edges {
          node {
            contentfulid
            title
            image {
              gatsbyImageData(
                layout: CONSTRAINED
                placeholder: BLURRED
                quality: 90
                height: 500
                width: 590
              )
            }
          }
        }
      }
    }
  `)
  const productCarousel = data.allContentfulHomePageCarousel
  return { productCarousel }
}

export default useProductCarousel
