import React from "react"
import Layout from "../components/UI/Layout/Layout"
import Landing from "../components/Home/Landing/Landing"
import Headline from "../components/Home/Headline/Headline"
import ProductCarousel from "../components/Home/ProductCarousel/ProductCarousel"
import Categories from "../components/Home/Categories/Categories"
import NewProducts from "../components/Home/NewProducts/NewProducts"
import StoreLocationSection from "../components/Home/StoreLocationSection"
import Discount from "../components/Home/Discount/Discount"
import PreFooter from "../components/UI/Footer/PreFooter"

const Home = () => {
  const handleRef = () => {
    window.scrollBy(0, window.innerHeight - 53 - window.pageYOffset)
  }

  return (
    <Layout>
      <Landing onClick={handleRef} />
      <Headline />
      <ProductCarousel />
      <Categories />
      <NewProducts />
      <Discount />
      <StoreLocationSection />
      <PreFooter/>
    </Layout>
  )
}

export default Home
